﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LobbyManager : NetworkBehaviour {

    private Text lobbyIP;
    private LobbyPlayerManager[] playersInLobby;
    public GameObject playerInLobbyPrefab;

    private void Awake()
    {
        if (Network.isClient)
        {
            GetComponentsInChildren<Text>()[0].gameObject.SetActive(false);
        }
        else
        {
            GetComponentsInChildren<Text>()[0].text += Network.player.ipAddress;
            GameObject.FindObjectOfType<NetworkMenuManager>().CreateLobby();
            if (Network.isClient)
            {
                //playersInLobby = 
            }
            else
            {
                playersInLobby = new LobbyPlayerManager[Network.maxConnections];
            }
            Debug.Log(Network.maxConnections);
            for (int index = 0; index < Network.maxConnections; index++)
            {
                playersInLobby[index] = ((GameObject)Network.Instantiate(playerInLobbyPrefab, transform.position, transform.rotation, 0)).GetComponent<LobbyPlayerManager>();
                playersInLobby[index].transform.SetParent(transform);
                playersInLobby[index].transform.localPosition -= new Vector3(0, 170 + 30 * index, 0);

            }
            if (Network.isServer)
            {
                playersInLobby[0].SetPlayerName(GameManager.GetGameManagerSingleton().GetPlayerName());
            }
        }
        Debug.Log("LOBBY: " + isClient);
        Debug.Log("LOBBY: " + isServer);
    }
    public void ShowNewPlayer(string newPlayerName)
    {
        for (int i = 0; i < Network.maxConnections; i++)
        {
            if (!playersInLobby[i].IsPlayerConnected())
            {
                playersInLobby[i].SetPlayerName(newPlayerName);
                return;
            }
        }
    }

    private void Start()
    {
    }
    
    private void OnConnectedToServer()
    {
        Debug.Log("CREATING LOBBY ON CLIENT");
        //GameObject gO = (GameObject)Network.Instantiate(lobbyMenu, transform.position, transform.rotation, 0);
        //gO.transform.SetParent(transform);
        //gO.SetActive(true);
        Debug.Log(isClient);
        //if (isClient)
        //{
        Debug.Log(GameManager.GetGameManagerSingleton().GetPlayerName());
        CmdRegisterPlayer(GameManager.GetGameManagerSingleton().GetPlayerName());
        RpcRegisterPlayer(GameManager.GetGameManagerSingleton().GetPlayerName());

        //}
    }
    private void OnPlayerConnected(NetworkPlayer player)
    {
        Debug.Log("CREATING LOBBY ON CLIENT");
        RpcRegisterPlayer(GameManager.GetGameManagerSingleton().GetPlayerName());
        CmdRegisterPlayer(GameManager.GetGameManagerSingleton().GetPlayerName());

    }
    [ClientRpc]
    public void RpcRegisterPlayer(string playerName)
    {
        Debug.Log("New PlayerConnected");
        ShowNewPlayer(playerName);
    }
    [Command]
    public void CmdRegisterPlayer(string playerName)
    {
        Debug.Log("New PlayerConnected");
        ShowNewPlayer(playerName);
    }
}
