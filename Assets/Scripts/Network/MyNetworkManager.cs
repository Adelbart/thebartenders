﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class MyNetworkManager : NetworkBehaviour
{
    public bool useNat;
    //private NetworkManager networkManager;
    //private ServerNetworkBehaviour serverManager;
    //private ClientNetworkBehaviour clientManager;
    private int desiredPort = 25002;

    private void Awake()
    {
        useNat = !Network.HavePublicAddress();
        //serverManager = GetComponent<ServerNetworkBehaviour>();
        //clientManager = GetComponent<ClientNetworkBehaviour>();
        //networkManager = GetComponent<NetworkManager>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    
    public NetworkConnectionError CreateServer(string serverName, string serverPassword, int numberOfPlayersExpected)
    {
        Network.incomingPassword = serverPassword;
        
        NetworkConnectionError nc = Network.InitializeServer(numberOfPlayersExpected, desiredPort, useNat);
        Debug.Log(nc.ToString());
        if (nc != NetworkConnectionError.NoError)
        {
            return nc;

        }

        //MasterServer.RegisterHost(GameManager.gameName, serverName, "");
        return NetworkConnectionError.NoError;
    }
    public NetworkConnectionError ConnectToServer(string ip, string password)
    {
        NetworkConnectionError nc = Network.Connect(ip, desiredPort, password);
        Debug.Log(nc.ToString());
        return nc;
    }
    

   
}
