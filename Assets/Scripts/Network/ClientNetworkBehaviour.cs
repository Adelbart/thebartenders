﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ClientNetworkBehaviour : NetworkBehaviour
{

    private void OnConnectedToServer()
    {
        Debug.Log(isLocalPlayer);
        //GameObject.FindObjectOfType<NetworkMenuManager>().CreateLobby();
        if (Network.isClient)
        {
            CmdRegisterPlayer(GameManager.GetGameManagerSingleton().GetPlayerName());
            RpcShowLobby();
            Debug.Log("Connected to server");
        }
    }
    private void OnFailedToConnect(NetworkConnectionError error)
    {
        //GameObject.FindObjectOfType<NetworkMenuManager>().
    }

    [Command]
    public void CmdRegisterPlayer(string playerName)
    {
        Debug.Log("New PlayerConnected");
        //FindObjectOfType<NetworkMenuManager>().ShowNewPlayer(playerName);
    }
    [ClientRpc]
    public void RpcShowLobby()
    {
        Debug.Log("Showing Lobby on clients");
        GameObject.FindObjectOfType<NetworkMenuManager>().CreateLobby();
    }
}
