﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServerNetworkBehaviour : NetworkBehaviour
{

    private void OnServerInitialized()
    {
        Debug.Log("ShowingLobby " +NetworkServer.active);
        GameObject.FindObjectOfType<NetworkMenuManager>().CreateLobby();
    }
    private void OnPlayerConnected(NetworkPlayer player)
    {
        RpcShowLobby();
        CmdRegisterPlayer("dupa");
        //CmdRegisterPlayer(GameManager.GetGameManagerSingleton().GetPlayerName());
        Debug.Log(player.ipAddress);
    }

    [Command]
    public void CmdRegisterPlayer(string playerName)
    {
        Debug.Log("New PlayerConnected "+playerName);
        //FindObjectOfType<NetworkMenuManager>().ShowNewPlayer(playerName);
    }
    [ClientRpc]
    public void RpcShowLobby()
    {
        Debug.Log("Showing Lobby on clients");
        GameObject.FindObjectOfType<NetworkMenuManager>().CreateLobby();
    }
}
