﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LevelManager : MonoBehaviour {


    public GameObject playerObject;
    public Player[] playersInGame;
    public GameObject buildMenu;
    public CameraManager camManager;
    private void Awake()
    {
        camManager = FindObjectOfType<CameraManager>();
        
    }
    // Use this for initialization
    void Start ()
    {
        InitializeLevel(GameManager.GetGameManagerSingleton().GetGameMode());
        camManager.InitializeCamera();
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            ShowBuildManager();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            SwitchCameraMode();
        }
    }

    void InitializeLevel(GameState.GameMode gM)
    {
        if(gM == GameState.GameMode.singlePlayer)
        {
            playersInGame = new Player[1];
        }

        for(int i = 0; i < playersInGame.Length; i++)
        {
            Debug.Log("Spawning player: " + i);
            playersInGame[i] = Instantiate(playerObject).GetComponent<Player>();
        }
    }
    void ShowBuildManager()
    {
        ShowBuildMenu();
    }
    void SwitchCameraMode()
    {
    }
    void ShowBuildMenu()
    {
        buildMenu.SetActive(!buildMenu.activeSelf);
    }
}
