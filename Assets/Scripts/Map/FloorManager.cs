﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorManager : MonoBehaviour {

    public int floorWidth = 20;
    public int floorHeight = 20;
    public GameObject floorPrefab;
    public GameObject floorHintPrefab;

    private FloorPanel[] floor;
    public GameObject floorHintParent;
    // Use this for initialization
    private void Awake()
    {
        floor = new FloorPanel[floorWidth * floorHeight];
        for (int i = 0; i < floorWidth; i++)
        {
            for (int j = 0; j < floorHeight; j++)
            {
                GameObject gO = Instantiate(floorPrefab);
                gO.transform.SetParent(this.transform);
                gO.transform.position += new Vector3(1 * j, 0, 1 * i);
                floor[i * floorWidth + j] = gO.GetComponent<FloorPanel>();

                gO = Instantiate(floorHintPrefab);
                gO.transform.SetParent(floorHintParent.transform);
                gO.transform.position += new Vector3(1 * j, 0.01f, 1 * i);
            }
        }
        HideFloorGrid();

    }
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            ToogleFloorGrid();
        }

    }

    public void ToogleFloorGrid()
    {
        floorHintParent.SetActive(!floorHintParent.activeSelf);
    }
    private void HideFloorGrid()
    {
        floorHintParent.SetActive(false);
    }
    private void ShowFloorGrid()
    {
        floorHintParent.SetActive(true);
    }
    public Vector3 GetFloorCentre()
    {
        Vector3 result = Vector3.zero;

        if (floorWidth % 2 == 0)
        {
            result.x = floor[floorWidth / 2].transform.position.x;
        }
        else
        {
            result.x = (floor[Mathf.FloorToInt(floorWidth / 2.0f)].transform.position.x + floor[Mathf.CeilToInt(floorWidth / 2.0f)].transform.position.x)/2;
        }
    
        if(floorHeight % 2 == 0)
        {
            result.z = floor[(floorHeight / 2) * floorWidth].transform.position.z;

        }
        else
        {
            result.x = (floor[Mathf.FloorToInt(floorWidth / 2.0f)*floorWidth].transform.position.z + floor[Mathf.CeilToInt(floorWidth / 2.0f) * floorWidth].transform.position.z) / 2;
        }


        return result;
    }
}
