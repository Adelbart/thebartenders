﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player : MonoBehaviour {

    private bool isActivePlayer;
    public NetworkIdentity networkIdentity;
    // Use this for initialization
    private void Awake()
    {
        isActivePlayer = true;

    }
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        MovePlayer();

	}
    public bool IsThisPlayerActive()
    {
        return isActivePlayer;
    }
    public void MovePlayer()
    {
        if((UserMovingDown() && UserMovingUp()) || (UserMovingLeft() && UserMovingRight()))
        {
            return;
        }
        Vector3 moveVector = Vector3.zero;
        if(UserMovingLeft() && UserMovingDown())
        {
            moveVector = (Vector3.left+Vector3.back);
        }
        else if(UserMovingLeft() && UserMovingUp())
        {
            moveVector = (Vector3.left + Vector3.forward);
        }
        else if(UserMovingRight() && UserMovingDown())
        {
            moveVector = (Vector3.right + Vector3.back);
        }
        else if(UserMovingRight() && UserMovingUp())
        {
            moveVector = (Vector3.right + Vector3.forward);
        }
        else if(UserMovingLeft())
        {
            moveVector = Vector3.left;
        }
        else if(UserMovingRight())
        {
            moveVector = Vector3.right;
        }
        else if (UserMovingDown())
        {
            moveVector = Vector3.back;
        }
        else if (UserMovingUp())
        {
            moveVector = Vector3.forward;
        }
        this.gameObject.transform.position += moveVector * Time.deltaTime * 4;
        FindObjectOfType<CameraManager>().MoveCameraAccordingToPlayer();
    }

    #region PlayerPossibleMoves

    private bool UserMovingLeft()
    {
        return (Input.GetKey(KeyCode.A) || Input.GetKeyDown(KeyCode.A));
    }
    private bool UserMovingRight()
    {
        return (Input.GetKey(KeyCode.D) || Input.GetKeyDown(KeyCode.D));
    }
    private bool UserMovingUp()
    {
        return (Input.GetKey(KeyCode.W) || Input.GetKeyDown(KeyCode.W));
    }
    private bool UserMovingDown()
    {
        return (Input.GetKey(KeyCode.S) || Input.GetKeyDown(KeyCode.S));
    }

    #endregion
}
