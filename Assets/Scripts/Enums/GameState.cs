﻿public static class GameState
{
    public enum GameMode
    {
        singlePlayer = 0,
        localCooperation = 1,
        networkCooperation = 2
    }
}