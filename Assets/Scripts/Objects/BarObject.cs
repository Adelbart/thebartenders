﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarObject:MonoBehaviour
{

    Vector2 floorGridPosition = Vector2.zero;
    bool isInteractable = false;

    public BarObject(Vector2 positionInGrid)
    {
        floorGridPosition = positionInGrid;
    }
}
