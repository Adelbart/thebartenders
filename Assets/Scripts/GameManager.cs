﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{

    // Use this for initialization

    private static GameManager gameManagerSingleton;
    private MyNetworkManager myNetworkManager;
    public static string gameName = "TheBartenders";
    private GameState.GameMode gameMode = GameState.GameMode.singlePlayer;
    private string playerName = "";

    private GameManager()
    {
        myNetworkManager = GameObject.FindObjectOfType<MyNetworkManager>();
    }
    public static GameManager GetGameManagerSingleton()
    {
        if(gameManagerSingleton == null)
        {
            gameManagerSingleton = new GameManager();
        }
        return gameManagerSingleton;
    }
    public MyNetworkManager GetMyNetworkManager()
    {
        return myNetworkManager;
    }
    public void SetMyNetworkManager(MyNetworkManager mymanager)
    {
        if (myNetworkManager == null)
        {
            myNetworkManager = mymanager;
        }
    }
    public GameState.GameMode GetGameMode()
    {
        return gameMode;
    }
    public void SetGameMode(GameState.GameMode gM)
    {
        gameMode = gM;
    }
    public void SetPlayerNickname(string nick)
    {
        playerName = nick;
    }
    public string GetPlayerName()
    {
        return playerName;
    }
}
