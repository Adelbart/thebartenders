﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    private Transform observedPlayerTransform;
    Camera gameCamera;
    public CameraEnums.CameraType currentCameraType = CameraEnums.CameraType.followingPlayer;
    Vector3 observedPoint;
    private void Awake()
    {
        gameCamera = GetComponent<Camera>();
    }

    void Start()
    {

    }

    // Use this for initialization
    public void InitializeCamera()
    {
        GetActivePlayer();
        if (currentCameraType == CameraEnums.CameraType.globalCamera)
        {
            SetCameraToWatchCentreOfFloor();
        }
        else
        {
            transform.localEulerAngles = new Vector3(60, 0, 0);
            SetCameraToWatchPlayerPosition();

        }
    }

    // Update is called once per frame
    void Update() {
        /*if (currentCameraType == CameraEnums.CameraType.followingPlayer)
        {
            MoveCameraAccordingToPlayer();
        }*/
        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            transform.localPosition += Vector3.back;
            transform.position += Vector3.forward;
        }
    }
    public void MoveCameraAccordingToPlayer()
    {
        if (this.observedPlayerTransform.position != observedPoint)
        {
            Vector3 positionDifference = this.observedPlayerTransform.position - observedPoint;
            this.transform.position += positionDifference;
            observedPoint = observedPlayerTransform.position;
        }

    }
    void SetCameraToWatchPlayerPosition()
    {
        observedPoint = observedPlayerTransform.position;

        Vector3 middleOfScreen = new Vector3(Screen.width, Screen.height, 0.0f);
        Vector3 middleOfScreenInWorldSpace = gameCamera.ScreenToWorldPoint(middleOfScreen);

        //Debug.Log(gameCamera.WorldToScreenPoint(observedPlayerTransform.position));
        //Debug.Log(middleOfScreenInWorldSpace);



        //Vector3 playerPosInScreen = gameCamera.WorldToScreenPoint(observedPlayerTransform.position);
        Vector3 playerPosInScreen = gameCamera.WorldToViewportPoint(observedPlayerTransform.position);

        /*do
        {
            
            if (playerPosInScreen.y < 0.5f)
            {
                this.transform.position += new Vector3(0,0,-0.1f);
            }
            else if (playerPosInScreen.y > 0.5f)
            {
                this.transform.position += new Vector3(0, 0, 0.1f);
            }
            playerPosInScreen = gameCamera.WorldToViewportPoint(observedPlayerTransform.position);
            Debug.Log(playerPosInScreen.y);
        }
        while ( playerPosInScreen.y <= 0.5f);*/

        //Debug.Log("Starting pos:" + playerPosInScreen);
        //Debug.Log("Starting camera pos:" + this.transform.position);
        do
        {
            if (playerPosInScreen.x < 0.5f)
            {
                this.transform.position += new Vector3(-0.1f, 0, 0);
            }
            else if (playerPosInScreen.x > 0.5f)
            {
                this.transform.position += new Vector3(0.1f, 0, 0);
            }
            if (playerPosInScreen.y < 0.5f)
            {
                this.transform.position += new Vector3(0, 0, -0.1f);
            }
            else if (playerPosInScreen.y > 0.5f)
            {
                this.transform.position += new Vector3(0, 0, 0.1f);
            }
            playerPosInScreen = gameCamera.WorldToViewportPoint(observedPlayerTransform.position);
            /*Debug.Log("Player pos:" + playerPosInScreen);
            Debug.Log("camera pos:" + this.transform.position);


            Debug.Log(Mathf.FloorToInt((playerPosInScreen.x * 10.0f) % 5) != 0);
            Debug.Log(Mathf.FloorToInt((playerPosInScreen.x * 10.0f) / 5) != 1);
            Debug.Log(playerPosInScreen.y);
            Debug.Log(Mathf.FloorToInt((playerPosInScreen.y * 10.0f) % 5) != 0);
            Debug.Log(Mathf.FloorToInt((playerPosInScreen.y * 10.0f) % 5));
            Debug.Log(Mathf.FloorToInt((playerPosInScreen.y * 10.0f) / 5) != 1);
            Debug.Log(Mathf.FloorToInt((playerPosInScreen.y * 10.0f) / 5));
            Debug.Log(Mathf.FloorToInt((playerPosInScreen.x * 10.0f) % 5) != 0 || Mathf.FloorToInt((playerPosInScreen.x * 10.0f) / 5) != 1 ||
                Mathf.FloorToInt((playerPosInScreen.y * 10.0f) % 5) != 0 || Mathf.FloorToInt((playerPosInScreen.y * 10.0f) / 5) != 1);*/
        }
        while (
                Mathf.FloorToInt((playerPosInScreen.x * 10.0f) % 5) != 0 || Mathf.FloorToInt((playerPosInScreen.x * 10.0f) / 5) != 1 ||
                Mathf.FloorToInt((playerPosInScreen.y * 10.0f) % 5) != 0 || Mathf.FloorToInt((playerPosInScreen.y * 10.0f) / 5) != 1
              );
        


        observedPoint = observedPlayerTransform.position;
        //this.transform.position = this.transform.position +(middleOfScreen- gameCamera.WorldToScreenPoint(observedPlayerTransform.position));
    }
    public void SetCameraToPosition(Vector3 newPosition)
    {
        transform.position = newPosition;
    }
    void GetActivePlayer()
    {
        foreach(Player p in FindObjectsOfType<Player>())
        {
            if (p.IsThisPlayerActive())
            {
                observedPlayerTransform = p.transform;
            }
        }
    }
    void SetCameraToWatchCentreOfFloor()
    {
        FloorManager floorManager = FindObjectsOfType<FloorManager>()[0];
        Vector3 centreOfFloor = floorManager.GetFloorCentre();
        gameCamera.transform.position = centreOfFloor + new Vector3(0, 30, 0);
        gameCamera.transform.LookAt(centreOfFloor);   
    }
}
