﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class NetworkMenuManager : NetworkBehaviour
{

    public GameObject networkModeSetup;
    public GameObject hostSetup;
    public GameObject hostConnectingSetup;
    public GameObject lobbyMenu;
    public GameObject myNetworkManager;

    public Text errorPrompter;


    // Use this for initialization
    void Start () {

        hostSetup.SetActive(false);
        networkModeSetup.SetActive(true);
        hostConnectingSetup.SetActive(false);
        MyNetworkManager myManager = Instantiate(myNetworkManager, NetworkManager.singleton.transform).GetComponent<MyNetworkManager>();
        GameManager.GetGameManagerSingleton().SetMyNetworkManager(myManager);
    }

    public void ShowHostSetupMenu()
    {
        networkModeSetup.SetActive(false);
        hostSetup.SetActive(true);
    }

    public void ShowConnectingSetup()
    {
        networkModeSetup.SetActive(false);
        hostConnectingSetup.SetActive(true);
    }
    public void TrySetupServer()
    {
        InputField[] inputs = GetComponentsInChildren<InputField>();
        if (string.IsNullOrEmpty(inputs[2].text))
        {
            errorPrompter.text = "Player name cannot be empty";
            return;
        }
            string desiredServerName = inputs[0].text;
            string desiredServerPassword = inputs[1].text;
            GameManager.GetGameManagerSingleton().SetPlayerNickname(inputs[2].text);
            Dropdown[] numberofPlayers = GetComponentsInChildren<Dropdown>();
            int desiredNumberOfPlayers = int.Parse(numberofPlayers[0].options[numberofPlayers[0].value].text);
            //Debug.Log(desiredServerName + "     "+desiredServerPassword+"       "+desiredNumberOfPlayers);

            NetworkConnectionError nc = GameManager.GetGameManagerSingleton().GetMyNetworkManager().CreateServer(desiredServerName, desiredServerPassword, desiredNumberOfPlayers);
        
        if (nc != NetworkConnectionError.NoError)
        {
            errorPrompter.text = nc.ToString();
        }
    }
    public void TryConnectingToServer()
    {
        InputField[] inputs = GetComponentsInChildren<InputField>();

        if (string.IsNullOrEmpty(inputs[2].text))
        {
            errorPrompter.text = "Player name cannot be empty";
            return;
        }

        string desiredServerIP = inputs[0].text;
        string desiredServerPassword = inputs[1].text;
        GameManager.GetGameManagerSingleton().SetPlayerNickname(inputs[2].text);

        NetworkConnectionError nc = GameManager.GetGameManagerSingleton().GetMyNetworkManager().ConnectToServer(desiredServerIP, desiredServerPassword);
        if(nc != NetworkConnectionError.NoError)
        {
            errorPrompter.text = nc.ToString();
        }
    }
    public void CreateLobby()
    {
        hostSetup.SetActive(false);
        networkModeSetup.SetActive(false);
        hostConnectingSetup.SetActive(false);
        lobbyMenu.SetActive(true);
        
        
    }

    
    private void OnServerInitialized()
    {
        Debug.Log("ShowingLobby " + isClient);
        GameObject gO = (GameObject)Network.Instantiate(lobbyMenu, transform.position, transform.rotation, 0);
        gO.transform.SetParent(transform);
        gO.SetActive(true);
    }

}
