﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class LobbyPlayerManager : NetworkBehaviour {
    
    private Text playerName;
    private Toggle playerReady;

    bool isConnected = false;

    // Use this for initialization
    private void Awake()
    {
        playerName = GetComponentInChildren<Text>();
        playerReady = GetComponentInChildren<Toggle>();
    }

    public void SetPlayerName(string name)
    {
        playerName.text = name;
        playerReady.transform.localPosition += new Vector3(playerName.rectTransform.sizeDelta.x, 0, 0);
        isConnected = true;
    }
    public bool IsPlayerConnected()
    {
        return isConnected;
    }
}
