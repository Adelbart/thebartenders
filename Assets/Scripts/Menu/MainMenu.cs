﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    // Use this for initialization
    public GameObject soloPlayMenu;
    public GameObject localCoopMenu;
    public GameObject networkCoopMenu;
    public GameObject mainMenu;
    
    public GameObject backButton;

    GameObject currentActiveObject;

    private void Awake()
    {
        currentActiveObject = mainMenu.activeSelf ? mainMenu : null;
        if(currentActiveObject == null)
        {
            mainMenu.SetActive(true);
            currentActiveObject = mainMenu;
        }
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SoloPlayChosen()
    {
        GameManager.GetGameManagerSingleton().SetGameMode(GameState.GameMode.singlePlayer);
        mainMenu.SetActive(false);
        soloPlayMenu.SetActive(true);
        backButton.SetActive(true);
        currentActiveObject = soloPlayMenu;
    }
    public void LocalPlayChosen()
    {
        GameManager.GetGameManagerSingleton().SetGameMode(GameState.GameMode.localCooperation);
        mainMenu.SetActive(false);
        localCoopMenu.SetActive(true);
        backButton.SetActive(true);
        currentActiveObject = localCoopMenu;
    }
    public void NetworkPlayChosen()
    {
        GameManager.GetGameManagerSingleton().SetGameMode(GameState.GameMode.networkCooperation);
        mainMenu.SetActive(false);
        currentActiveObject = Instantiate(networkCoopMenu,transform);
        //currentActiveObject = Network.Instantiate(networkCoopMenu,Vector3.zero,Quaternion.identity,0);
        backButton.SetActive(true);
    }
    public void BackButtonClicked()
    {
        backButton.SetActive(false);
        DestroyImmediate(currentActiveObject);
        mainMenu.SetActive(true);
        currentActiveObject = mainMenu;
    }
    

    #region GameStarts

    public void SoloGameStart()
    {
        SceneManager.LoadScene(1);
    }
    public void LocalGameStart()
    {
    }
   
    public void TrySetupServer()
    {
    }
    public void NetworkGameStartJoin()
    {

    }
    #endregion
}
